const initialState = {
  pools: {
    0: {
      secret: 0,
      vote: 0,
      numTokens: 0
    }
  },
  votingRights: 0,
  lastPool: 0
}

const PLCRVReducer = (state = initialState, action) => {
  if (
    action.type === 'PLCRV_VOTED' ||
    action.type === 'PLCRV_START_POOL' ||
    action.type === 'PLCRV_VOTING_RIGHTS' ||
    action.type === 'PLCRV_VOTING' ||
    action.type === 'PLCRV_REVEALING'
  ) {
    return { ...state, ...action.payload }
  }

  return state
}

export default PLCRVReducer
