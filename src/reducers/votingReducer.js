const initialState = {
  candidates: {},
  voted: false
}

const votingReducer = (state = initialState, action) => {
  if (action.type === 'VOTING_VOTED') {
    return { ...state, ...action.payload }
  } else if (action.type === 'VOTING_FETCH') {
    return { ...state, ...action.payload }
  }

  return state
}

export default votingReducer
