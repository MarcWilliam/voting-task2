import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './user/userReducer'
import web3Reducer from './util/web3/web3Reducer'
import votingReducer from './reducers/votingReducer';

import PLCRVReducer from './reducers/PLCRVReducer';

const reducer = combineReducers({
  routing: routerReducer,
  user: userReducer,
  web3: web3Reducer,
  voting: votingReducer,
  PLCRV: PLCRVReducer
})

export default reducer
