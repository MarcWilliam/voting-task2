import store from '../../store'
import Web3 from 'web3'

export const WEB3_INITIALIZED = 'WEB3_INITIALIZED'
function web3Initialized(results) {
  return {
    type: WEB3_INITIALIZED,
    payload: results
  }
}

let getWeb3 = new Promise(function (resolve, reject) {
  // Wait for loading completion to avoid race conditions with web3 injection timing.
  resolve(store.dispatch(web3Initialized({
    web3Instance: new Web3(Web3.givenProvider || "http://localhost:8545")
  })))
})

export default getWeb3
