import React, { Component } from 'react'
import VotingFormContainer from '../../ui/VotingForm/VotingFormContainer'

class Voting extends Component {
  constructor(props) {
    super(props)
    console.log(props);
  }

  render() {
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Voting</h1>
            <VotingFormContainer />
          </div>
        </div>
      </main>
    )
  }
}

export default Voting;
