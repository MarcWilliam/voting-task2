import React, { Component } from 'react'

import RequestVRFormContainer from '../../ui/RequestVRForm/RequestVRFormContainer';
import WithdrawVRFormContainer from '../../ui/withdrawVRForm/WithdrawVRFormContainer';
import StartPoolFormContainer from '../../ui/StartPoolForm/StartPoolFormContainer';
import CommitVoteFormContainer from "../../ui/CommitVoteForm/CommitVoteFormContainer";
import RevealVoteFormContainer from '../../ui/RevealVoteForm/RevealVoteFormContainer';

class PLCRVoting extends Component {
  constructor(props) {
    super(props)
    console.log(props);
  }

  render() {
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">

            <h1>PLCRVoting</h1>

            <RequestVRFormContainer />
            <WithdrawVRFormContainer />
            <StartPoolFormContainer />
            <CommitVoteFormContainer />
            <RevealVoteFormContainer />

          </div>
        </div>
      </main>
    )
  }
}

export default PLCRVoting;
