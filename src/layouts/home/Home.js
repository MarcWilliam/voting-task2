import React, { Component } from 'react'

class Home extends Component {
  render() {
    return(
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Good to Go!</h1>
            <p>visit Voting or PLCR Voting to start</p>
          </div>
        </div>
      </main>
    )
  }
}

export default Home
