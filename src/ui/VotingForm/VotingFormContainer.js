import { connect } from 'react-redux'
import VotingForm from './VotingForm'
import { voteVoting, fetchCandidates } from './VotingFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    candidates: state.voting.candidates,
    voted: state.voting.voted
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onVotingFormVote: (candidate) => dispatch(voteVoting(candidate)),
    fetchCandidates: () => dispatch(fetchCandidates())
  }
}

const VotingFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(VotingForm)

export default VotingFormContainer
