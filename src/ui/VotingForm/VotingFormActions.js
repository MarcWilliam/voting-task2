import VotingContractABI from '../../../build/contracts/Voting.json';
import store from '../../store'

export const VOTING_VOTED = 'VOTING_VOTED'
function votingVoted(candidate) {
  let candidates = { ...store.getState().voting.candidates };
  candidates[candidate]++;
  return {
    type: VOTING_VOTED,
    payload: {
      ...store.getState().voting,
      candidates: candidates,
      voted: true
    }
  }
}

export function voteVoting(candidate) {

  let web3 = store.getState().web3.web3Instance


  // Double-check web3's status.
  if (typeof web3 !== 'undefined') {
    return function (dispatch) {

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(VotingContractABI.abi, VotingContractABI.networks[networkID].address);
          console.log("accounts", web3.utils.utf8ToHex(candidate));
          contract.methods.voteForCandidate(web3.utils.utf8ToHex(candidate)).send({
            from: accounts[0]
          }).then((data) => {
            alert('Voted successfully with transaction Hash ' + data.transactionHash)
            console.log(data);
          });

          alert("Waiting for transaction ...")
        });
      });

      dispatch(votingVoted(candidate))
    }
  } else {
    console.error('Web3 is not initialized.');
  }
}

export const VOTING_FETCH = 'VOTING_FETCH'
function votingFetch(candidates) {
  return {
    type: VOTING_FETCH,
    payload: {
      ...store.getState().voting,
      candidates: candidates
    }
  }
}

export function fetchCandidates() {

  let web3 = store.getState().web3.web3Instance
  console.log(web3)
  // Double-check web3's status.
  if (web3 !== 'undefined') {
    return function (dispatch) {

      var candidates = {};

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(VotingContractABI.abi, VotingContractABI.networks[networkID].address);
          contract.methods.getCandidateList().call().then((data) => {
            var fetched = 0;
            data.forEach(candidateName => {
              contract.methods.totalVotesFor(candidateName).call().then((votes) => {
                candidates[web3.utils.hexToUtf8(candidateName)] = votes;
                if (++fetched >= data.length) {
                  dispatch(votingFetch(candidates))
                }
              });
            });
          });
        });
      });

    }
  } else {
    console.error('Web3 is not initialized.');
  }
}
