import React, { Component } from 'react'

class VotingForm extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.fetchCandidates();
  }

  handleVote(e, candidate) {
    this.props.onVotingFormVote(candidate);
  }

  render() {
    return (
      <table className="pure-table pure-table-horizontal">

        <thead>
          <tr>
            <th>Name</th>
            <th>Votes</th>
            {this.props.voted ? null : (<th></th>)}
          </tr>
        </thead>

        <tbody>
          {Object.keys(this.props.candidates).map(name => {
            return (
              <tr key={name} style={{ height: "55px" }}>
                <td>{name}</td>
                <td>{this.props.candidates[name]}</td>
                {this.props.voted ? null : (<td><button className="pure-button" onClick={e => this.handleVote(e, name)}>Vote</button></td>)}
              </tr>
            );
          })}
        </tbody>

      </table>
    )
  }
}

export default VotingForm
