import { connect } from 'react-redux'
import RevealVoteForm from './RevealVoteForm'
import { revealVote } from './RevealVoteFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    votingRights: state.PLCRV.votingRights
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    revealVote: (pollID, voteOption, salt) => dispatch(revealVote(pollID, voteOption, salt))
  }
}

const RevealVoteFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RevealVoteForm)

export default RevealVoteFormContainer
