import PLCRVotingABI from '../../../build/contracts/PLCRVoting.json';
import store from '../../store'


export const PLCRV_REVEALING = 'PLCRV_REVEALING'
function PLCRVoting(pollID) {
  const state = store.getState().PLCRV;
  return {
    type: PLCRV_REVEALING,
    payload: {
    ...state
    }
  }
}

export function revealVote(pollID, voteOption, salt) {
  var state = store.getState();
  let web3 = state.web3.web3Instance;
  let lastPool = state.PLCRV.lastPool;
  // Double-check web3's status.
  if (web3 !== 'undefined') {
    return function (dispatch) {

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(PLCRVotingABI.abi, PLCRVotingABI.networks[networkID].address);

          contract.methods.revealVote(pollID, voteOption, salt).send({
            from: accounts[0]
          }).then((data) => {
            console.log(data);
            alert("Revealed Successfully");
            dispatch(PLCRVoting(pollID));
          });

        });
      });
    }

  } else {
    console.error('Web3 is not initialized.');
  }
}
