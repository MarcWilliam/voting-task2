import React, { Component } from 'react'

class RevealVoteForm extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() { }

  handleSubmit(e) {
    e.preventDefault();
    this.props.revealVote(
      e.target.elements.pollID.value,
      e.target.elements.voteOption.value,
      e.target.elements.salt.value
    );
  }

  render() {
    return (
      <form className="pure-form pure-form-aligned" onSubmit={(e) => this.handleSubmit(e)}>
        <fieldset>
          <legend>Reveal Vote</legend>

          <div className="pure-control-group">
            <label htmlFor="pollID">Poll ID</label>
            <input id="pollID" required type="number" placeholder="Pool ID" />
          </div>
          <div className="pure-control-group">
            <label htmlFor="salt">Secret</label>
            <input id="salt" required type="number" placeholder="Tokens Amount" />
          </div>
          <div className="pure-control-group">
            <label htmlFor="voteOption">Vote Option</label>
            <input id="voteOption" required type="number" placeholder="Vote Option" />
          </div>
          <div className="pure-controls">
            <button type="submit" className="pure-button pure-button-primary">Submit</button>
          </div>
        </fieldset>
      </form>
    )
  }
}

export default RevealVoteForm
