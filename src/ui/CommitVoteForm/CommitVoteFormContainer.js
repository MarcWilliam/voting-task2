import { connect } from 'react-redux'
import CommitVoteForm from './CommitVoteForm'
import { commitVote } from './CommitVoteFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    votingRights: state.PLCRV.votingRights
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    commitVote: (pollID, numTokens, voteOption) => dispatch(commitVote(pollID, numTokens, voteOption))
  }
}

const CommitVoteFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CommitVoteForm)

export default CommitVoteFormContainer
