import PLCRVotingABI from '../../../build/contracts/PLCRVoting.json';
import store from '../../store'


export const PLCRV_VOTING = 'PLCRV_VOTING'
function PLCRVoting(pollID, secret, numTokens, voteOption) {
  const state = store.getState().PLCRV;
  return {
    type: PLCRV_VOTING,
    payload: {
      ...state,
      pools: {
        ...state.pools, [pollID]: {
          secret: secret,
          numTokens: numTokens,
          voteOption: voteOption
        },
        lastPool: pollID
      }
    }
  }
}

export function commitVote(pollID, numTokens, voteOption) {
  var state = store.getState();
  let web3 = state.web3.web3Instance;
  let lastPool = state.PLCRV.lastPool;
  // Double-check web3's status.
  if (web3 !== 'undefined') {
    return function (dispatch) {

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(PLCRVotingABI.abi, PLCRVotingABI.networks[networkID].address);

          var secret = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
          var secretHash = web3.utils.sha3(web3.utils.toHex(voteOption) + web3.utils.toHex(secret), { encoding: "hex" });

          contract.methods.commitVote(pollID, secretHash, numTokens, lastPool).send({
            from: accounts[0]
          }).then((data) => {
            console.log(data);
            alert("Voted Successfully, you secret is " + secret);
            dispatch(PLCRVoting(pollID, secret, numTokens, voteOption));
          })

        });
      });
    }

  } else {
    console.error('Web3 is not initialized.');
  }
}
