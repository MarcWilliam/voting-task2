import React, { Component } from 'react'

class StartPoolForm extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() { }

  handleSubmit(e) {
    e.preventDefault();
    this.props.startPool(
      e.target.elements.voteQuorum.value,
      e.target.elements.commitDuration.value,
      e.target.elements.revealDuration.value
    );
  }

  render() {
    return (
      <form className="pure-form pure-form-aligned" onSubmit={(e) => this.handleSubmit(e)}>
        <fieldset>
          <legend>Start Pool</legend>

          <div className="pure-control-group">
            <label htmlFor="voteQuorum">Vote Quorum</label>
            <input id="voteQuorum" required type="number" placeholder="Vote Quorum" />
          </div>
          <div className="pure-control-group">
            <label htmlFor="commitDuration">Commit Duration</label>
            <input id="commitDuration" required type="number" placeholder="Commit Duration in sec" />
          </div>
          <div className="pure-control-group">
            <label htmlFor="revealDuration">Reveal Duration</label>
            <input id="revealDuration" required type="number" placeholder="Reveal Duration in sec" />
          </div>
          <div className="pure-controls">
            <button type="submit" className="pure-button pure-button-primary">Submit</button>
          </div>
        </fieldset>
      </form>
    )
  }
}

export default StartPoolForm
