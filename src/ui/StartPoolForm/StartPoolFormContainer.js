import { connect } from 'react-redux'
import StartPoolForm from './StartPoolForm'
import { startPool } from './StartPoolFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    pools: state.PLCRV.pools
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    startPool: (voteQuorum, commitDuration, revealDuration) => dispatch(startPool(voteQuorum, commitDuration, revealDuration))
  }
}

const StartPoolFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StartPoolForm)

export default StartPoolFormContainer
