import PLCRVotingABI from '../../../build/contracts/PLCRVoting.json';
import store from '../../store'


export const PLCRV_START_POOL = 'PLCRV_START_POOL'
function PLCRStartPool(pool, poolID) {
  const state = store.getState().PLCRV;
  return {
    type: PLCRV_START_POOL,
    payload: {
      ...state,
      pools: { ...state.pools, [poolID]: pool }
    }
  }
}

export function startPool(voteQuorum, commitDuration, revealDuration) {

  let web3 = store.getState().web3.web3Instance
  // Double-check web3's status.
  if (web3 !== 'undefined') {
    return function (dispatch) {

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(PLCRVotingABI.abi, PLCRVotingABI.networks[networkID].address);
          console.log(voteQuorum, commitDuration, revealDuration);
          contract.methods.startPoll(voteQuorum, commitDuration, revealDuration).send({
            from: accounts[0]
          }).then((data) => {
            let poolID = data.events._PollCreated.returnValues.pollID;
            console.log(data, poolID);
            alert("Started a new pool with id " + poolID);

            dispatch(PLCRStartPool(poolID));
          })


        });
      });
    }

  } else {
    console.error('Web3 is not initialized.');
  }
}
