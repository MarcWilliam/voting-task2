import React, { Component } from 'react'

class RequestVRForm extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() { }

  handleSubmit(e) {
    e.preventDefault();
    this.props.requestVotingRights(e.target.elements.amount.value);
  }

  render() {
    return (
      <form className="pure-form pure-form-aligned" onSubmit={(e) => this.handleSubmit(e)}>
        <fieldset>
          <legend>Request Voting Rights</legend>
          <div className="pure-control-group">
            <label htmlFor="amount">Token Amount</label>
            <input id="amount" required type="number" placeholder="Token Amount" />
          </div>
          <div className="pure-controls">
            <button type="submit" className="pure-button pure-button-primary">Submit</button>
          </div>
        </fieldset>
      </form>
    )
  }
}

export default RequestVRForm
