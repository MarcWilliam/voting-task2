import { connect } from 'react-redux'
import RequestVRForm from './RequestVRForm'
import { requestVotingRights } from './RequestVRFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    votingRights: state.PLCRV.votingRights
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    requestVotingRights: (amount) => dispatch(requestVotingRights(amount))
  }
}

const RequestVRFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestVRForm)

export default RequestVRFormContainer
