import PLCRVotingABI from '../../../build/contracts/PLCRVoting.json';
import TokenABI from '../../../build/contracts/EIP20Interface.json'
import store from '../../store'


export const PLCRV_VOTING_RIGHTS = 'PLCRV_VOTING_RIGHTS'
function PLCRVotingRights(votingRights) {
  const state = store.getState().PLCRV;
  return {
    type: PLCRV_VOTING_RIGHTS,
    payload: {
      ...store.getState().PLCRV,
      votingRights: state.votingRights + votingRights
    }
  }
}

export function withdrawVotingRights(amount) {

  let web3 = store.getState().web3.web3Instance
  // Double-check web3's status.
  if (web3 !== 'undefined') {
    return function (dispatch) {

      web3.eth.net.getId().then((networkID) => {
        web3.eth.getAccounts().then((accounts) => {
          var contract = new web3.eth.Contract(PLCRVotingABI.abi, PLCRVotingABI.networks[networkID].address);
          var tokenContract = new web3.eth.Contract(TokenABI.abi, TokenABI.networks[networkID].address);
          //console.log(tokenContract, contract);
          //contract.methods.token().call(console.log);

          contract.methods.withdrawVotingRights(amount).send({
            from: accounts[0]
          }).then((voteData) => {
            console.log(voteData);
            dispatch(PLCRVotingRights(-amount));
          });

        });
      });
    }

  } else {
    console.error('Web3 is not initialized.');
  }
}
