import { connect } from 'react-redux'
import WithdrawVRForm from './WithdrawVRForm'
import { withdrawVotingRights } from './WithdrawVRFormActions'

const mapStateToProps = (state, ownProps) => {
  return {
    votingRights: state.PLCRV.votingRights
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    withdrawVotingRights: (amount) => dispatch(withdrawVotingRights(amount))
  }
}

const WithdrawVRFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WithdrawVRForm)

export default WithdrawVRFormContainer
